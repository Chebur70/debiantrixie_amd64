#!/bin/bash
#Скрипт по пакетной установки программ с возможностью их выбора 
#Автор: Александр Клич, сайт https://prostolinux.my1.ru
#"Спасибо klichalex автору скрипта Transformation! https://www.youtube.com/user/klichalex/featured"
#"Спасибо klichalex автору хороших видео уроков! https://rutube.ru/channel/23628980/videos/"
#"Спасибо klichalex автору перевода Refracta на Русский язык http://prostolinux.my1.ru/"
#Дополнение chebur chebur3133@gmail.com https://prostolinux.my1.ru/index/st/0-11 https://gitlab.com/Chebur70"
#"Спасибо circulosmeos https://github.com/circulosmeos за предоставленые скрипты"
#"Спасибо разработчику скриптов Dmitriy wj42ftns Chekhov, Russia https://gist.github.com/wj42ftns"
#"Спасибо kachnu https://github.com/kachnu за предоставленые скрипты"
#"Спасибо Systemback https://gitlab.com/Kendek/systemback"
#"Спасибо LeCorbeau's Vault https://lecorbeausvault.wordpress.com/2021/01/10/quickly-build-a-custom-bootable-installable-debian-live-iso-with-live-build/"
#"Спасибо Франко Кониди https://syslinuxos.com/things-to-do-after-installing-syslinuxos-12/"
#"Нет недостижимых целей,есть высокий коэффициент лени,недостаток смекалкии и запас отговорок"
#"Не важно насколько медленно ты движешься, главное не останавливаться.Конфуций"
#"Наш ответ Чемберлену!Дави Империализма Гиену Могучий Рабочий Класс! Вчера были танки лишь у Чемберлена,А нынче есть и у нас!"
#"Запомните,чтобы ничего не делать, надо уметь делать все" 
#"Я не потерпел неудачу, я нашел 10 000 способов, которые не сработают-Эдисон."

cd ~

#Добавление архитектуры i386
sudo dpkg --add-architecture i386 
sudo apt update && sudo apt full-upgrade -y
sudo apt autoremove -y 
#Установка обязательных программ
sudo apt install -y sudo curl wget apt-transport-https geany gdebi dirmngr aptitude engrampa onboard unzip 
#Удаление старых настроек 
rm -Rf ~/.config/xfce4/
#Удоление папки autostart
rm -Rf ~/.config/autostart/
#Добавление папки autostart
mkdir ~/.config/autostart 
#Добавление папки applications
mkdir ~/.local/share/applications 
#Appearance
wget https://gitlab.com/Chebur70/Setting/-/raw/main/appearance.tar.gz
tar xvf appearance.tar.gz && rm -rf appearance.tar.gz
chmod -R 777 appearance
#Applications
wget https://gitlab.com/Chebur70/Setting/-/raw/main/applications.tar.gz 
tar xvf applications.tar.gz && rm -rf applications.tar.gz
chmod -R 777 applications
rm -rf applications/org.gtkhash.gtkhash.desktop
mv applications/gparted.desktop ~/.local/share/applications/
sudo cp -R applications /usr/share/ && rm -Rf applications
#Icons
wget https://gitlab.com/Chebur70/Setting/-/raw/main/icons.tar.gz
tar xvf icons.tar.gz && rm -rf icons.tar.gz
chmod -R 777 icons
sudo cp -R icons /usr/share/ && rm -Rf icons
#Themes
wget https://gitlab.com/Chebur70/Setting/-/raw/main/themes.tar.gz
tar xvf themes.tar.gz && rm -rf themes.tar.gz
chmod -R 777 themes
sudo cp -R themes /usr/share/ && rm -Rf themes
#Backgrounds
wget https://gitlab.com/Chebur70/Setting/-/raw/main/backgrounds.tar.gz
tar xvf backgrounds.tar.gz && rm -rf backgrounds.tar.gz
chmod -R 777 backgrounds
sudo cp backgrounds/05.jpg /boot/grub
sudo update-grub
sudo cp -R backgrounds /usr/share/ && rm -Rf backgrounds
#Install gdebi от Klichalex
wget https://gitlab.com/Chebur70/Setting/-/raw/main/gdebi.desktop.tar.gz
tar xvf gdebi.desktop.tar.gz && rm -rf gdebi.desktop.tar.gz
sudo mv gdebi.desktop /usr/share/applications/
# Установка программ по выбору.
sudo apt install -y dialog
echo "================================="
echo "ВАШ БЕСПРОВОДНОЙ КОНТРОЛЕР,запомните его."
echo "(Если строка ниже пустая, то его нет)"
lspci | grep -i Network
echo "================================="
sleep 13s

cmd=(dialog --separate-output --checklist "Выберите программное обеспечение, которое вы хотите установить:" 22 76 16)
	options=(1 "Grub themes Rosa" on  #любой параметр может быть установлен по умолчанию на "off"
		  2 "Plymout themes Percentage" off
		  3 "QtFsarchiver.Appimage-утилита для клонирования диска под Linux" on
		  4 "BackUpRestoreAppImage-7.0-системы Linux" on
		  5 "Systemback-Резервное копирование" on
		  6 "Slimjet-быстрый и безопасный веб-браузер" off
		  7 "PaleMoon-браузер" off
		  8 "GoogleChrome-stable-браузер" off
		  9 "Chromium-браузер" off
		10 "Opera-stable-браузер" off
		11 "Vivaldi-браузер" off
		12 "Minbrowser-браузер" off
		13 "Brave-браузер с открытым исходным кодом" off
		14 "Torbrowser-мощный инструмент для защиты приватности и онлайновых свобод" off
		15 "LibreWolf-Браузер, фокусирующийся на безопасности" off
		16 "Gvidm-утилита для быстрого изменения разрешения экрана" on
		17 "Qshutdown-утилита для отключения компьютера" on
		18 "Bucklespring-консольная утилита для эмуляции "щелчков" механической клавиатуры " on
		19 "ThunarOfRoot-Работа с архивами в xfce4" on
		20 "CompizEmerald-композитный менеджер" on
		21 "Stacer-Оптимизация, очистка, настройка системы" on
		22 "BleachBit-инструмент,освободит диск ,обеспечив безопасность данных" on
		23 "GKrellM-виджет системных мониторов-Linux" on
		24 "RamboxAppimage-управляйте всеми своими коммуникационными аккаунтами из этого приложения" off
		25 "FranzAppimage-приложение для обмена gdown.pl-1.4сообщениями" off
		26 "SkypeWeb-мессенджер" off
		27 "ZoomAppimage-Коференция" off
		28 "TelegramWeb-мессенджер" on
		29 "Signal-мессенджер" on
		30 "WireWeb-приложение для зашифрованной связи" on
		31 "WhatsAppWeb-мессенджер" on
		32 "ViberAppimage-мессенджер" on
		33 "SMSMessagesWeb-сообщения на компьютере" off
		34 "GoogleTranslateWeb-позволяет мгновенно переводить слова" on
		35 "SpeedtestNetWeb-скорость интернета и задержку соединения." off
		36 "Любая Ваша программа" off
		37 "BauhAppimage-магазин приложений AppImage, AUR, Flatpaks и Snaps для Linux" on
		38 "Anydesk-удаленная помощь" off
		39 "Любая Ваша программа" off
		40 "FreeTubeAppimage-Частный клиент YouTube" off
		41 "Cherrytree-Приложение для создания иерархических заметок" on
		42 "MxBootRepairAppimage-Восстановление загрузки" on
		43 "MXBootOptionsAppimage-Параметры загрузки" off
		44 "MXMenuEditorAppimage-Меню-редактор" off
		45 "ddCopy-создания загрузочных Live USB, копирования ISO-образов" off
		46 "UNetbootin-Утилита для создания Live USB" off
		47 "EtcherAppimage-предназначенное для записи файлов образов дисков" off
		48 "RosaImageWriter-Запись ISO-образов на USB-диск" on
		49 "LumQtAppimage-Запись ISO-образов на USB-накопители" on
		50 "MultiBootUSB-создания мультизагрузочных USB-носителей" on
		51 "Ventoy-создание мультизагрузочной флешки" on
		52 "Mintstick-Графический интерфейс для записи файлов .img или .iso на USB" on
		53 "CapacityTester-Проверка USB-накопителя или карты памяти,является ли это поддельной" off
		54 "DDRescue-GUI-инструмент для восстановления данных" off
		55 "Любая Ваша программа" off
		56 "Любая Ваша программа" off
		57 "Любая Ваша программа" off
		58 "Любая Ваша программа" off
		59 "Любая Ваша программа" off
		60 "Любая Ваша программа" off
		61 "Любая Ваша программа" off
		62 "Любая Ваша программа" off
		63 "Любая Ваша программа" off
		64 "Любая Ваша программа" off
		65 "Любая Ваша программа" off
		66 "Любая Ваша программа" off
		67 "Любая Ваша программа" off
		68 "Cutter-это бесплатная платформа обратного проектирования" off
		69 "Gotop-монитор графической активности на основе терминалов" on
		70 "gImageReader-извлечение текста из изображений и PDF-файлов в Linux" on
		71 "Дополнительные драйвера Wi-Fi Marvell и NXP (Libertas" off
                72 "Дополнительные драйвера Wi-Fi Intel" off 
                73 "Дополнительные драйвера Wi-Fi Realtek" off
                74 "Дополнительные драйвера Wi-Fi Atheros" off
                75 "Дополнительные драйвера Wi-Fi Broadcom" off
                76 "ClipGrabAppimage-это бесплатный загрузчик и конвертор видео из YouTube" off
                77 "XtremeDownloadManager-зaгрузчик файлов" off
                78 "YoutubeDl+YoutubeDlGui+YtDlp-Консольный+Графический интерфейс медиа-загрузчика" on
                79 "YTFZF-программа для поиска,просмотра и скачивания с YouTube в терминале" off
                80 "JDownloader-это бесплатный инструмент управления загрузками" off
                81 "VideomassApp-это кроссплатформенный графический интерфейс для FFMPEG и YouTube-DL / YT-DLP" off
                82 "MediaDownloader-Загружает видео" off
                83 "MotrixAppimage-загружайте большие файлы без сбоев" off
                84 "Persepolis-менеджер загрузок и графический интерфейс для Aria2" off
                85 "AppImageUpdate-обновляет на основе информации встроенной в AppImage" off
                86 "Detect It Easy-Определяет тип файла в Linux" off
                87 "Любая Ваша программа" off
                88 "Pluma-легковерный текстовый редактор" off
                89 "MeditAppImage-это более быстрый аналог Gedit" on
                90 "Leafpad-текстовый редактор" off
                91 "Gedit-свободный текстовый редактор" off
                92 "OpenOfficeAppimage-свободный пакет офисных приложений, основан на коде StarOffice" off
                93 "FreeofficeAppimage-Бесплатный полнофункциональный офисный пакет" off
                94 "OnlyOfficeAppimage-офисный пакет с открытым исходным кодом" off
                95 "WPSOfficeAppimage-офисный пакет" off
                96 "Conky-мощный и легко настраиваемый системный монитор" off
                97 "GnomePie-лаунчер для быстрого запуска других программ" off
                98 "Plank-простая док-панель" off
                99 "Любая Ваша программа" off
              100 "Речевой Сервер Speech Dispatcher" off
              101 "Из каталога deb-Любые debпакеты с зависимостями что закинете" off)

		choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
		clear
		for choice in $choices
		do
		    case $choice in
	        	      1)
	            		#Install GrubThemesRosa
				echo "================ Установка GrubThemesRosa ================"								 
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/Rosa.tar.gz
                                tar xvf Rosa.tar.gz && rm -rf Rosa.tar.gz   
                                sudo mkdir /boot/grub/themes/                                                          
                                sudo mv rosa /boot/grub/themes/rosa                                                                                                                                                                                                                                                         
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/GrubRosa.tar.gz
                                tar xvf GrubRosa.tar.gz && rm -rf GrubRosa.tar.gz                                    
                                sudo mv grub /etc/default/grub                                                          
                                sudo update-grub                        
				;;
			     2)
				#Install PlymoutThemesPercentage
				echo "================ Установка PlymoutThemesPercentage ================"  
                                sudo apt install -y plymouth-themes                                                                                                                                                                                                                                                                                                                                                    
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/GrubRosa.tar.gz
                                tar xvf GrubRosa.tar.gz && rm -rf GrubRosa.tar.gz     
                                sudo mv grub /etc/default/grub 
				wget https://gitlab.com/Chebur70/Setting/-/raw/main/Percentage.tar.gz
                                tar xvf Percentage.tar.gz && rm -rf Percentage.tar.gz                                                                
                                sudo mv percentage /usr/share/plymouth/themes/percentage                                                                                        
                                sudo update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth \
				/usr/share/plymouth/themes/percentage/percentage.plymouth 01
                                sudo plymouth-set-default-theme -R percentage
                                sudo update-initramfs -u -k all
                                sudo update-grub                                                                                                                      
				;;
			     3)
                                #Install QtFsarchiver.Appimage
				echo "================ Установка QtFsarchiver.Appimage ================"
                                wget https://www.dropbox.com/sh/nhk6o4ju8ct3itt/AABHc_HVirrG92klpURYt3UUa?dl=0 && \
				unzip -q 'AABHc_HVirrG92klpURYt3UUa?dl=0'                             
                                chmod +x QtFsarchiver/QtFsarchiverAppImage.desktop                                                         
                                sudo mv QtFsarchiver/QtFsarchiverAppImage.desktop /usr/share/applications/
                                sudo mv QtFsarchiver/QtFsarchiver.png /usr/share/icons/   
				chmod +x QtFsarchiver/QtFsarchiver.AppImage                        
                                sudo mv QtFsarchiver/QtFsarchiver.AppImage /opt/
				mv QtFsarchiver/QtFsarchiver.sh ~/'Рабочий стол'/		
                                rm -rf 'AABHc_HVirrG92klpURYt3UUa?dl=0' && rm -Rf QtFsarchiver
				;;
			     4)
				#Install BackUpRestore.AppImage-7.0
			        echo "================ Установка BackUpRestore.AppImage-7.0 ================"
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/BackUpRestoreAplPng.tar.gz	
			        tar xvf BackUpRestoreAplPng.tar.gz && rm -rf BackUpRestoreAplPng.tar.gz
				chmod +x BackUpRestoreAplPng/BackUpRestore.AppImage
                                sudo mv BackUpRestoreAplPng/BackUpRestore.AppImage /opt/	       
			        chmod +x BackUpRestoreAplPng/BackUpRestoreAppImage.desktop
			        sudo mv BackUpRestoreAplPng/BackUpRestoreAppImage.desktop /usr/share/applications/
			        sudo mv BackUpRestoreAplPng/BackUpRestore.png /usr/share/icons/		       
			        rm -Rf BackUpRestoreAplPng   
				;;
			     5)
				#Install SystembackSh
				echo "================ Установка SystembackSh================"
				sudo apt install -y attr psmisc rsync xterm				 
                                wget https://www.dropbox.com/sh/md6xjgfvxt6g6zg/AADKC7zWE4hZqaKKoCKJmF0ea?dl=0 && \
				unzip -q 'AADKC7zWE4hZqaKKoCKJmF0ea?dl=0'	
				chmod +x systemback/systemback.sh
				sudo cp systemback/systemback.sh /usr/local/bin/
				chmod +x systemback/RestoreSystemback.desktop
				chmod +x systemback/BackUpSystemback.desktop
				sudo mv systemback/RestoreSystemback.desktop /usr/share/applications/
                                sudo mv systemback/BackUpSystemback.desktop /usr/share/applications/
                                sudo mv systemback/systemback.png /usr/share/icons/
				mv systemback/SystembackREADME.sh ~/'Рабочий стол'/
				sudo mkdir /home/sblive                                                        
                                rm -rf 'AADKC7zWE4hZqaKKoCKJmF0ea?dl=0' && rm -Rf systemback
				;;
			     6)
				#Install SlimjetDeb
			        echo "================ Установка SlimjetDeb ================"
                                wget https://www.dropbox.com/sh/cvwwvvvsdjh98jh/AACjiNduWLJJbgqqcHVSqPNpa?dl=0 && \
				unzip -q 'AACjiNduWLJJbgqqcHVSqPNpa?dl=0'
				sudo dpkg -i Slimjet/slimjet*.deb
                                sudo apt install -fy                  
				tar xvf Slimjet/slimjet.tar.gz    
                                mv slimjet ~/.config/slimjet
                                rm -rf 'AACjiNduWLJJbgqqcHVSqPNpa?dl=0' && rm -Rf Slimjet                                                                                                 
				;;
			     7)
				#Install PaleMoonTar
				echo "================ Установка PaleMoonTar ================"
                                wget https://www.dropbox.com/sh/had6irovtbdciqq/AAAM-XymjB8FxiHQT-ILBfcZa?dl=0 && \
				unzip -q 'AAAM-XymjB8FxiHQT-ILBfcZa?dl=0'
				tar xvf PaleMoon/palemoon*.tar.xz
				sudo mv palemoon /opt/palemoon
				sudo chown -R $USER:$USER /opt/palemoon
                                chmod +x PaleMoon/PaleMoonTar.desktop
                                sudo cp PaleMoon/PaleMoonTar.desktop /usr/share/applications/ 
                                sudo cp PaleMoon/Palemoon.png /usr/share/icons/
				tar xvf PaleMoon/MoonchildProductions.tar.gz
                                chmod u+x '.moonchild productions'
                                rm -rf 'AAAM-XymjB8FxiHQT-ILBfcZa?dl=0' && rm -Rf PaleMoon	 
				;;
			     8) 
				#Install GoogleChromeStable 
		                echo "================ Установка GoogleChromeStable ================"
				wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
                                sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" \
				>> /etc/apt/sources.list.d/google.list'
                                sudo apt update && sudo apt install -y google-chrome-stable  		 
				wget https://www.dropbox.com/sh/0wi9h2a7ej64xz7/AAAQNJGMkPEt1huEQJ9esjjPa?dl=0 && \
				unzip -q 'AAAQNJGMkPEt1huEQJ9esjjPa?dl=0'      				 
				chmod +x GoogleChrome/google-chrome.desktop                                                                                                                
                                sudo cp GoogleChrome/google-chrome.desktop /usr/share/applications/
                                sudo cp GoogleChrome/GoogleChrome.png /usr/share/icons/
                                tar xvf GoogleChrome/google-chrome.tar.gz 
                                mv google-chrome ~/.config/google-chrome
                                rm -rf 'AAAQNJGMkPEt1huEQJ9esjjPa?dl=0' && rm -Rf GoogleChrome                                 
                                echo 'Очистка дублей репозитериев (Chrome - создаёт дубли)'  
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/aptsources-cleanup.pyz
                                printf 'yes\n' | sudo python3 -OEs aptsources-cleanup.pyz && sudo rm -rf aptsources-cleanup.pyz         
				;;
			     9)
				#Install Chromium
				echo "================ Установка Chromium ================"
				sudo apt install -y chromium chromium-l10n chromium-sandbox                                 
				wget https://www.dropbox.com/sh/qkm84kodwizr3q4/AAB2tzdNJ4g7oB_Vu68Zy-1Wa?dl=0 && \
				unzip -q 'AAB2tzdNJ4g7oB_Vu68Zy-1Wa?dl=0'
                                chmod +x Chromium/chromium.desktop
                                sudo cp Chromium/chromium.desktop /usr/share/applications/  
                                sudo cp Chromium/Chromium.png /usr/share/icons/ 
                                tar xvf Chromium/chromium.tar.gz   
                                cp -r chromium ~/.config/ && rm -rf chromium                         
                                rm -rf 'AAB2tzdNJ4g7oB_Vu68Zy-1Wa?dl=0' && rm -Rf Chromium                                           
				;; 
			   10) 
				#Install OperaDeb 
		                echo "================ Установка Opera ================"
                                wget https://www.dropbox.com/sh/f49m4vq27ytehz4/AACfwkqNO6CcizvhJElUIsqIa?dl=0 && \
				unzip -q 'AACfwkqNO6CcizvhJElUIsqIa?dl=0'
				sudo dpkg -i Opera/opera-*
                                sudo apt install -fy				
                                chmod +x Opera/opera.desktop
                                cp Opera/opera.desktop ~/.local/share/applications/ 
                                sudo cp Opera/Opera.svg /usr/share/icons/      
                                tar xvf Opera/opera.tar.gz    
                                mv opera ~/.config/opera                     
                                rm -rf 'AACfwkqNO6CcizvhJElUIsqIa?dl=0' && rm -Rf Opera                                
				;;
			   11)
				#Install VivaldiSh          
                                echo "================ Установка VivaldiSh ================"                                                   
				wget https://www.dropbox.com/sh/2en0ti85y9mr4zc/AADXX_KNMM9t-vQnl-Gd_ewMa?dl=0 && \
				unzip -q 'AADXX_KNMM9t-vQnl-Gd_ewMa?dl=0'
				tar xvf Vivaldi/vivaldi-snapshot.tar.gz
				mv vivaldi-snapshot .config/vivaldi-snapshot
				chmod +x Vivaldi/install-vivaldi.sh
                                sh Vivaldi/install-vivaldi.sh				
				rm -rf 'AADXX_KNMM9t-vQnl-Gd_ewMa?dl=0' && rm -Rf Vivaldi                                 
				;;
			   12)
                                #Install MinBrowserAppimage
				echo "================ Установка MinBrowserAppimage ================"
                                wget https://www.dropbox.com/sh/5179t2mpm71b685/AADn5KYTquvcyQf_o-OAlHQNa?dl=0 && \
				unzip -q 'AADn5KYTquvcyQf_o-OAlHQNa?dl=0'                                                                               
                                chmod +x MinAppimage/MinApp.desktop
                                sudo mv MinAppimage/MinApp.desktop /usr/share/applications/
                                sudo mv MinAppimage/Min.png /usr/share/icons/ 
				chmod +x ./MinAppimage/*.AppImage
				sudo mv MinAppimage/Min.AppImage /opt/                         
				tar xvf MinAppimage/Min.tar.gz
				mv Min ~/.config/Min				
                                rm -rf 'AADn5KYTquvcyQf_o-OAlHQNa?dl=0' && rm -Rf MinAppimage		 
				;;
			   13)
				#Install BraveBrowserApt
		                echo "================ Установка BraveBrowserApt  ================"
                                sudo apt install -y curl
                                sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg \
				https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
				echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg] \
				https://brave-browser-apt-release.s3.brave.com/ stable main"\
				|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
				sudo apt update && sudo apt install -y brave-browser
				wget https://www.dropbox.com/sh/6g531ht3q5nt9hd/AAAh45XaZU9LhSqLTojHkWkRa?dl=0 && \
				unzip -q 'AAAh45XaZU9LhSqLTojHkWkRa?dl=0' 
				tar xvf Brave/BraveSoftware.tar.gz
				mv BraveSoftware ~/.config/BraveSoftware
                                rm -rf 'AAAh45XaZU9LhSqLTojHkWkRa?dl=0' && rm -Rf Brave
				;;
			   14)
				#Install TorBrowser
				echo "================ Установка TorBrowser ================"      
				wget https://www.dropbox.com/sh/3zfwfebyq47t7td/AAB8BZOjc6G66GYuODc6KCi8a?dl=0 && \
				unzip -q 'AAB8BZOjc6G66GYuODc6KCi8a?dl=0'		
				tar xvf Tor/tor-browser.tar.gz
				sudo mv tor-browser/start-tor-browser.desktop /usr/share/applications/
				sudo mv tor-browser  /opt/tor-browser
				sudo chown -R $USER:$USER /opt/tor-browser 				
				rm -rf 'AAB8BZOjc6G66GYuODc6KCi8a?dl=0' && rm -Rf Tor	 
				;;
			   15)
				#Install LibreWolfAppimage
				echo "================ Установка LibreWolfAppimage =============="  
                                wget https://www.dropbox.com/sh/xdh6sx560o105lg/AABIuMvChhQ2DrYk6PFJxrSUa?dl=0 && \
				unzip -q 'AABIuMvChhQ2DrYk6PFJxrSUa?dl=0'                                                                             
                                chmod +x LibreWolfApp/LibreWolfApp.desktop
                                sudo mv LibreWolfApp/LibreWolfApp.desktop /usr/share/applications/   
                                sudo mv LibreWolfApp/LibreWolf.png /usr/share/icons/ 				
                                chmod +x ./LibreWolfApp/*.AppImage
				sudo mv LibreWolfApp/LibreWolf*.AppImage /opt/LibreWolf.AppImage
				tar xvf LibreWolfApp/librewolf.tar.gz
                                rm -rf 'AABIuMvChhQ2DrYk6PFJxrSUa?dl=0' && rm -Rf LibreWolfApp	                    
				;;
			   16)
				#Install Gvidm
				echo "================ Установка Gvidm =============================="
				sudo apt install -y gvidm
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/GvidmAplPng.tar.gz
				tar xvf GvidmAplPng.tar.gz && rm -rf GvidmAplPng.tar.gz 			 
                                chmod +x GvidmAplPng/gvidm.desktop
                                sudo cp GvidmAplPng/gvidm.desktop /usr/share/applications/
				sudo cp GvidmAplPng/Gvidm.png /usr/share/icons/                           
				rm -Rf GvidmAplPng                                                                                                                                 
				;;
			   17)
				#Install Qshutdown
			        echo "================ Установка Qshutdown ================"
                                sudo apt install qshutdown -y
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/QshutdownAplPng.tar.gz
				tar xvf QshutdownAplPng.tar.gz && rm -rf QshutdownAplPng.tar.gz
                                chmod +x QshutdownAplPng/qshutdown.desktop
                                sudo cp QshutdownAplPng/qshutdown.desktop /usr/share/applications/
                                sudo cp QshutdownAplPng/Qshutdown.png /usr/share/icons/
				rm -Rf QshutdownAplPng  
				;;
			   18)
				#Install Bucklespring
			        echo "================ Установка Bucklespring ================"
			        sudo apt install -y bucklespring     
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/BucklespringAplPng.tar.gz
				tar xvf BucklespringAplPng.tar.gz && rm -rf BucklespringAplPng.tar.gz
                                chmod +x BucklespringAplPng/Bucklespring.desktop         
                                sudo cp BucklespringAplPng/Bucklespring.desktop /usr/share/applications/             
                                sudo cp BucklespringAplPng/Bucklespring.desktop /etc/xdg/autostart/
				sudo cp BucklespringAplPng/Bucklespring.png /usr/share/icons/  
				rm -rf BucklespringAplPng/                               
				;;
			   19)
				#Install ThunarOfRoot-Работа с архивами в xfce4
			        echo "================ Установка ThunarOfRoot-Работа с архивами в xfce4 ================"
                                sudo apt install -y ark engrampa file-roller p7zip atool minizip zenity lzma pdlzip pbzip2 r-cran-zip rzip xarchiver tar \
				unar unrar-free thunar-archive-plugin youtube-dl ffmpeg  
				wget https://gitlab.com/Chebur70/Setting/-/raw/main/rar_6.20-0.1_amd64.deb 
				sudo dpkg -i rar*.deb
				sudo apt install -fy && rm -rf rar*                                           
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/ThunarOfRoot.tar.gz && tar xvf ThunarOfRoot.tar.gz 
                                tar xvf ThunarOfRoot/ThunarOfRoot.desktop.tar.gz                         
                                chmod +x ThunarOfRoot.desktop
                                sudo mv ThunarOfRoot.desktop /usr/share/applications/ 
                                tar xvf ThunarOfRoot/ThunarGeany.tar.gz
                                cp -R Thunar ~/.config/ && rm -Rf Thunar
                                tar xvf ThunarOfRoot/youtube2what.tar.gz   
				chmod +x youtube2what 
				sudo mv youtube2what /usr/local/bin/
                                tar xvf ThunarOfRoot/icons.tar.gz
                                chmod -R 777 icons
                                sudo cp -R icons /usr/share/ && rm -Rf icons
                                echo "===== GeanyApl ==================="
                                tar xvf ThunarOfRoot/geany.desktop.tar.gz
                                chmod +x geany.desktop
                                sudo mv geany.desktop /usr/share/applications/                                 
                                rm -rf ThunarOfRoot.tar.gz && rm -Rf ThunarOfRoot                                                                                                                         				                                                                				 			                        
				;;
			   20)
				#Install CompizEmerald
			        echo "================ Установка CompizEmerald ================"
                                sudo apt install -y compiz compiz-core compiz-plugins compiz-plugins-default compiz-plugins-extra \
				compiz-plugins-main compizconfig-settings-manager emerald emerald-themes
		                wget https://gitlab.com/Chebur70/Setting/-/raw/main/CompizEmeraldAplPng.tar.gz
				tar xvf CompizEmeraldAplPng.tar.gz && rm -rf CompizEmeraldAplPng.tar.gz
                                chmod -R 777 CompizEmeraldAplPng
                                chmod +x CompizEmeraldAplPng/emerald-theme-manager.desktop
                                sudo cp CompizEmeraldAplPng/Compiz_off.desktop /usr/share/applications/ && \
				sudo cp CompizEmeraldAplPng/Compiz_on.desktop /usr/share/applications/                              
                                sudo cp CompizEmeraldAplPng/ccsm.desktop /usr/share/applications/  && \
				sudo cp CompizEmeraldAplPng/Emerald_on.desktop /usr/share/applications/  
                                sudo cp CompizEmeraldAplPng/emerald-theme-manager.desktop /usr/share/applications/                                                                                            
                                #cp CompizEmeraldAplPng/Emerald_on.desktop ~/.config//autostart/ && \
				#cp CompizEmeraldAplPng/Compiz_on.desktop ~/.config//autostart/  
                                chmod -R 777 CompizEmeraldAplPng/icons                             
                                sudo cp -R CompizEmeraldAplPng/icons /usr/share/           
				tar xvf CompizEmeraldAplPng/compiz.tar.gz  
                                mv compiz ~/.config/compiz      
				tar xvf CompizEmeraldAplPng/emerald.tar.gz
				rm -Rf CompizEmeraldAplPng          
				;;
			  21)
				#Install Stacer
			        echo "================ Установка Stacer ================"
                                sudo apt install -y stacer       
				;;
			  22)
                                #Install BleachBit
			        echo "================ Установка BleachBit ================"   
                                sudo apt install -y bleachbit  
                                wget https://www.dropbox.com/sh/6db8pm1e530401b/AADO3fX5Jw1HNYt1WH9aAPJpa?dl=0 && \
				unzip -q 'AADO3fX5Jw1HNYt1WH9aAPJpa?dl=0'
                                chmod +x Bleachbit/bleachbit-root.desktop
			        sudo cp Bleachbit/bleachbit-root.desktop /usr/share/applications/
                                sudo cp Bleachbit/Bleachbit.png /usr/share/icons/
                                rm -rf 'AADO3fX5Jw1HNYt1WH9aAPJpa?dl=0' && rm -Rf Bleachbit          	 
			       ;;
			  23)
			        #Install Gkrellm
			        echo "================ Установка Gkrellm ================"
			        sudo apt install -y gkrellm 
				wget https://www.dropbox.com/sh/wafutq4xlczbza0/AAAy1l-wqCiLabOmhD82uCN8a?dl=0 && \
				unzip -q 'AAAy1l-wqCiLabOmhD82uCN8a?dl=0'
                                chmod +x Gkrellm/gkrellm.desktop
                                mv Gkrellm/gkrellm.desktop ~/.config/autostart/
				tar xvf Gkrellm/Bgkrellm2.tar.gz
                                rm -rf 'AAAy1l-wqCiLabOmhD82uCN8a?dl=0' && rm -Rf Gkrellm                                                                
			       ;;
			  24)
			        #Install RamboxAppimage
			        echo "================ Установка RamboxAppimage ================"	
				wget https://www.dropbox.com/sh/4j27qqksnmsq9wn/AADjG8vhqn64jId8QeazMOdRa?dl=0 && \
				unzip -q 'AADjG8vhqn64jId8QeazMOdRa?dl=0'
                                chmod +x Rambox/RamboxApp.desktop
				sudo mv Rambox/RamboxApp.desktop /usr/share/applications/
				sudo mv Rambox/Rambox.png /usr/share/icons/
				chmod +x ./Rambox/*.AppImage
                                sudo mv Rambox/Rambox*.AppImage /opt/Rambox.AppImage
				rm -rf 'AADjG8vhqn64jId8QeazMOdRa?dl=0' && rm -Rf Rambox                                           
			       ;;
			  25)
			        #Install FranzAppimage
				echo "================ Установка FranzAppimage ================"
                                wget https://www.dropbox.com/sh/pm6hq9npw8jahvw/AADwWMEDOrwkht9unmc9vExYa?dl=0 && \
				unzip -q 'AADwWMEDOrwkht9unmc9vExYa?dl=0'
                                chmod +x Franz/FranzApp.desktop
				sudo mv Franz/FranzApp.desktop /usr/share/applications/
				sudo mv Franz/FranzApp.png /usr/share/icons/
				chmod +x ./Franz/*.AppImage
                                sudo mv Franz/Franz*.AppImage /opt/Franz.AppImage
				rm -rf 'AADwWMEDOrwkht9unmc9vExYa?dl=0' && rm -Rf Franz                                                             
			       ;;
			  26)
			        #Install SkypeWeb
		                echo "================ Установка SkypeWeb ================"
				wget https://www.dropbox.com/sh/d15l1f1brdh8608/AAAdFTVBEeM66CReApIlLCjRa?dl=0 && \
				unzip -q 'AAAdFTVBEeM66CReApIlLCjRa?dl=0'
                                chmod +x Skype/SkypeF.desktop
				sudo mv Skype/SkypeF.desktop /usr/share/applications/
				sudo mv Skype/Skype.png /usr/share/icons/
				rm -rf 'AAAdFTVBEeM66CReApIlLCjRa?dl=0' && rm -Rf Skype
			       ;;
		          27)
			        #Install Zoom.AppImage
				echo "================ Установка Zoom.AppImage ================"
				wget https://www.dropbox.com/sh/9t72pow785wpceu/AAAScxcu5xmrmup57YfakUQsa?dl=0 && \
				unzip -q 'AAAScxcu5xmrmup57YfakUQsa?dl=0'
                                chmod +xZoom/ZoomApp.desktop
				sudo mv Zoom/ZoomApp.desktop /usr/share/applications/
				sudo mv Zoom/Zoom.png /usr/share/icons/
				chmod +x ./Zoom/*.AppImage
                                sudo mv Zoom/Zoom*.AppImage /opt/Zoom.AppImage
				rm -rf 'AAAScxcu5xmrmup57YfakUQsa?dl=0' && rm -Rf Zoom
			       ;;
			  28)
			        #Install TelegramTsetup
		                echo "================ Установка TelegramTsetup ================"
				wget https://www.dropbox.com/sh/s3ybf17voaga97i/AAD1q9E-oOOZ39h7o_RpPZ_ca?dl=0 && \
				unzip -q 'AAD1q9E-oOOZ39h7o_RpPZ_ca?dl=0'
				tar xvf TelegraM/tsetup*.tar.xz
				sudo mv Telegram /opt/Telegram
                                chmod +x TelegraM/org.telegram.desktop._3e485da34fc040f9218e3891ecde1e6c.desktop
				chmod +x TelegraM/Updater.desktop
				cp TelegraM/org.telegram.desktop._3e485da34fc040f9218e3891ecde1e6c.desktop ~/.local/share/applications/
				sudo mv TelegraM/Updater.desktop /usr/share/applications/
				sudo mv TelegraM/Telegram.png /usr/share/icons/
				sudo mv TelegraM/Updater.png /usr/share/icons/
				rm -rf 'AAD1q9E-oOOZ39h7o_RpPZ_ca?dl=0' && rm -Rf TelegraM
		               ;;
			  29)
			        #Install SignalApt
				echo "================  Установка SignalApt ================"				
                                wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg
                                cat signal-desktop-keyring.gpg | sudo tee /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null
                                echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' |\
                                sudo tee /etc/apt/sources.list.d/signal-xenial.list
				rm -rf signal-desktop-keyring.gpg
                                sudo apt update && sudo apt install -y signal-desktop
				wget https://www.dropbox.com/sh/nszgf7iaeqluw33/AABeHiSD0fTZWifAOGWnWx01a?dl=0 && \
				unzip -q 'AABeHiSD0fTZWifAOGWnWx01a?dl=0'
				chmod +x Signal/signal-desktop.desktop
                                sudo mv Signal/signal-desktop.desktop /usr/share/applications/
                                sudo mv Signal/Signal.png /usr/share/icons/				
                                rm -rf 'AABeHiSD0fTZWifAOGWnWx01a?dl=0' rm -Rf Signal
			       ;;
			  30)
			        #Install WireWeb
		                echo "================ Установка WireWeb ================"
                                wget https://www.dropbox.com/sh/cchy00tljr431il/AABPjdODeAHavyraKdqF91-ba?dl=0 && \
				unzip -q 'AABPjdODeAHavyraKdqF91-ba?dl=0'
                                chmod +x Wire/WireF.desktop
				sudo mv Wire/WireF.desktop /usr/share/applications/
				sudo mv Wire/Wire.png /usr/share/icons/
				rm -rf 'AABPjdODeAHavyraKdqF91-ba?dl=0' && rm -Rf Wire                    
			       ;;
                          31)
			        #Install WhatsAppDeb
		                echo "================ Установка WhatsAppDeb ================"
                                wget https://www.dropbox.com/sh/itnkex987dv63jh/AADCkyWHaZq9cfhBb0QBghlqa?dl=0 && \
				unzip -q 'AADCkyWHaZq9cfhBb0QBghlqa?dl=0'				
                                sudo dpkg -i Whatsapp/whatsapp*amd64.deb
				sudo apt install -fy
				sudo mv Whatsapp/Whatsapp.png /usr/share/icons/ 
				chmod +x Whatsapp/com.github.eneshecan.WhatsAppForLinux.desktop
				sudo cp Whatsapp/com.github.eneshecan.WhatsAppForLinux.desktop /usr/share/applications/                                                             
				rm -rf 'AADCkyWHaZq9cfhBb0QBghlqa?dl=0' && rm -Rf Whatsapp                            
			       ;;
			  32)
		                #Install ViberAppImage
                                echo "================ Установка ViberAppImage ================"
                                wget https://www.dropbox.com/sh/5c88ucnin6uae0h/AABQCi4foO5jVO-9AXS-unXea?dl=0 && \
				unzip -q 'AABQCi4foO5jVO-9AXS-unXea?dl=0'
                                chmod +x Viber/ViberApp.desktop
				sudo mv Viber/ViberApp.desktop /usr/share/applications/
				sudo mv Viber/Viber.png /usr/share/icons/
				chmod +x ./Viber/*.AppImage
                                sudo mv Viber/viber.AppImage /opt/
				rm -rf 'AABQCi4foO5jVO-9AXS-unXea?dl=0' && rm -Rf Viber
			       ;;
			  33)
			        #Install SMS-Messages
				echo "================  Установка SMS-Messages ================"
                                wget https://www.dropbox.com/sh/yqw1z8v8fexxrho/AADV8N4OEgkmQxbxz70yvNBea?dl=0 && \
				unzip -q 'AADV8N4OEgkmQxbxz70yvNBea?dl=0'
                                chmod +x SMSMessages/SMSMessagesF.desktop
				sudo mv SMSMessages/SMSMessagesF.desktop /usr/share/applications/
				sudo mv SMSMessages/SMSMessages.png /usr/share/icons/
				rm -rf 'AADV8N4OEgkmQxbxz70yvNBea?dl=0' && rm -Rf SMSMessages             		              
			       ;;
			  34)
			        #Install GoogleTranslateWeb
			        echo "================ Установка GoogleTranslateWeb ================"
                                wget https://www.dropbox.com/sh/fj0l3tn6crbsfyg/AABqfHbT3o40SvyfD9yprFD-a?dl=0 && \
				unzip -q 'AABqfHbT3o40SvyfD9yprFD-a?dl=0'
                                chmod +x GoogleTranslate/GoogleTranslateF.desktop
				sudo mv GoogleTranslate/GoogleTranslateF.desktop /usr/share/applications/
				sudo mv GoogleTranslate/GoogleTranslate.png /usr/share/icons/                  
                                rm -rf 'AABqfHbT3o40SvyfD9yprFD-a?dl=0' && rm -Rf GoogleTranslate	                                            
			       ;;
			  35)
			        #Install SpeedtestWeb
		                echo "================ Установка SpeedtestWeb ================"
                                wget https://www.dropbox.com/sh/vysj5fkvnxkmeii/AAA5l_9P6bGWtOCMj0ng4nD1a?dl=0 && \
				unzip -q 'AAA5l_9P6bGWtOCMj0ng4nD1a?dl=0'
                                chmod +x Speedtest/SpeedtestF.desktop
				sudo mv Speedtest/SpeedtestF.desktop /usr/share/applications/
				sudo mv Speedtest/Speedtest.png /usr/share/icons/
                                rm -rf 'AAA5l_9P6bGWtOCMj0ng4nD1a?dl=0' && rm -Rf Speedtest	  
			       ;;
			  36)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"
		 	       ;;			
			  37)
			        #Install BauhAppImage
			        echo "================ Установка BauhAppImage ================"	
                                sudo apt-get install -y python3 python3-pip python3-yaml python3-dateutil \
				python3-pyqt5 python3-packaging python3-requests                                
				wget https://www.dropbox.com/sh/3o0is46szlbc7jp/AABXRmQ_h8fNx6uAFbpj84dua?dl=0 && \
				unzip -q 'AABXRmQ_h8fNx6uAFbpj84dua?dl=0'                                            
                                mv Bauh/Bauh.sh ~/'Рабочий стол'/                      
                                chmod +x Bauh/BauhApp.desktop
                                sudo mv Bauh/BauhApp.desktop /usr/share/applications/ 
                                sudo mv Bauh/Bauh.svg /usr/share/icons/ 
                                chmod +x ./Bauh/*.AppImage
				sudo mv Bauh/bauh*.AppImage /opt/bauh.AppImage                            
                                rm -rf 'AABXRmQ_h8fNx6uAFbpj84dua?dl=0' && rm -Rf Bauh
			       ;;
			  38)
			        #Install AnydeskDeb
				echo "================ Установка AnydeskDeb ================"
                                wget https://www.dropbox.com/sh/1vvd99olkmtraih/AACM_x29BDmbtW7CtaglwFwfa?dl=0 && \
				unzip -q 'AACM_x29BDmbtW7CtaglwFwfa?dl=0'
                                sudo dpkg -i AnyDesk/anydesk*
                                sudo apt install -fy		                                
                                rm -rf 'AACM_x29BDmbtW7CtaglwFwfa?dl=0' && rm -Rf AnyDesk			 			 
			       ;;
			  39)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================" 
			       ;;
			  40)
			        #Install FreeTubeAppImage
		                echo "================ Установка FreeTubeAppImage ================"
				wget https://www.dropbox.com/sh/n78t9ogufcpbiw0/AAB7HYdU94o_7BKIP1KUPGjWa?dl=0 && \
				unzip -q 'AAB7HYdU94o_7BKIP1KUPGjWa?dl=0'
                                chmod +x FreeTubeApp/FreeTubeApp.desktop
                                sudo mv FreeTubeApp/FreeTubeApp.desktop /usr/share/applications/
                                sudo mv FreeTubeApp/FreeTube.png /usr/share/icons/				 				
                                chmod +x ./FreeTubeApp/*.AppImage
                                sudo mv FreeTubeApp/freetube*.AppImage /opt/freetube.AppImage
				tar xvf FreeTubeApp/FreeTube.tar.gz
				mv FreeTube ~/.config/FreeTube
				rm -rf 'AAB7HYdU94o_7BKIP1KUPGjWa?dl=0' && rm -Rf FreeTubeApp
			       ;;
			  41)
			        #Install Cherrytree
			        echo "================ Установка Cherrytree ================"
				sudo apt-get install -y cherrytree                                                            				
				wget https://www.dropbox.com/sh/ev9n5ttb3cf99u7/AAAWZl4luvIIk3BJEfX-eRTWa?dl=0 && \
				unzip -q 'AAAWZl4luvIIk3BJEfX-eRTWa?dl=0'                   				                                
                                chmod -R 777 Cherrytree                         
                                sudo cp Cherrytree/Cherrytree.svg /usr/share/icons/  
                                sudo cp Cherrytree/cherrytree.desktop /usr/share/applications/
				cp Cherrytree/ПаролиLiveссылки.ctb ~/'Рабочий стол'/
                                cp -r Cherrytree/cherrytree ~/.config/  
                                rm -rf 'AAAWZl4luvIIk3BJEfX-eRTWa?dl=0' && rm -Rf Cherrytree
			       ;;
			  42)
                               #Install MxBootRepairAppimage
			       echo "================ Установка MxBootRepairAppimage ================"
			       wget https://www.dropbox.com/sh/90uazf02iu0qc4o/AADREKkXv7VF6R-espdXDbeYa?dl=0 && \
			       unzip -q 'AADREKkXv7VF6R-espdXDbeYa?dl=0'			       
			       chmod +x MxBootRepair/MxBootRepair.desktop
			       sudo mv MxBootRepair/MxBootRepair.desktop /usr/share/applications/
			       sudo mv MxBootRepair/MxBootRepair.png /usr/share/icons/
			       chmod +x ./MxBootRepair/*.AppImage
                               sudo mv MxBootRepair/MXBootRepair.AppImage /opt/
			       rm -rf 'AADREKkXv7VF6R-espdXDbeYa?dl=0' && rm -Rf MxBootRepair
			       ;;
			  43)
				#Install MXBootOptionsAppimage
				echo "================ Установка MXBootOptionsAppimage ================"
				wget https://www.dropbox.com/sh/sppi7c0e1cy8r42/AAAhlc194CCNoCJkZ5vClkpua?dl=0 && \
				unzip -q 'AAAhlc194CCNoCJkZ5vClkpua?dl=0'
				chmod +x MXBoot/MXBootOptions.AppImage	
				sudo mv MXBoot/MXBootOptions.AppImage /opt/
				chmod +x MXBoot/MXBootOptions.desktop
                                sudo mv MXBoot/MXBootOptions.desktop /usr/share/applications/ 
                                sudo mv MXBoot/MXBootOptions.png /usr/share/icons/ 						                                
				rm -rf 'AAAhlc194CCNoCJkZ5vClkpua?dl=0' && rm -Rf MXBoot 
				;;
			   44)
				#Install MXMenuEditorAppimage
				echo "================ Установка MXMenuEditorAppimage ================"
				wget https://www.dropbox.com/sh/qmn5jr7364ig42j/AAC0yA8V5bs8alDmANaMJX0Ca?dl=0 && \
				unzip -q 'AAC0yA8V5bs8alDmANaMJX0Ca?dl=0'
                                chmod +x MXMenu/MXMenuEditor.AppImage	
				sudo mv MXMenu/MXMenuEditor.AppImage /opt/
				chmod +x MXMenu/MXMenuEditor.desktop
                                sudo mv MXMenu/MXMenuEditor.desktop /usr/share/applications/ 
                                sudo mv MXMenu/MXMenuEditor.png /usr/share/icons/ 
				rm -rf 'AAC0yA8V5bs8alDmANaMJX0Ca?dl=0' && rm -Rf MXMenu
				;;
			   45)
				#Install ddCopy
			        echo "================ Установка ddCopy ================"                            
			        wget https://www.dropbox.com/sh/t4b3rja97a04tsc/AAAEyWsv5YAGcujFRnX6vGjYa?dl=0 && \
				unzip -q 'AAAEyWsv5YAGcujFRnX6vGjYa?dl=0'
			        sudo dpkg -i ddCopy/ddcopy*
			        sudo apt install -fy  
                                rm -rf 'AAAEyWsv5YAGcujFRnX6vGjYa?dl=0' && rm -Rf ddCopy                             
				;;
			   46)
				#Install Unetbootin 
			        echo "================ Установка Unetbootin ================"
                                sudo apt install -y xclip mtools extlinux
                                wget https://www.dropbox.com/sh/71p463jvkqkzmpz/AACuBEC58iO1vLxoUz5FuZKOa?dl=0 && \
				unzip -q 'AACuBEC58iO1vLxoUz5FuZKOa?dl=0'			                                
                                chmod +x Unetbootin/unetbootin*.bin
                                sudo mv Unetbootin/unetbootin*.bin /opt/unetbootin.bin                                				 
				chmod +x Unetbootin/Unetbootin.desktop                                                
				sudo cp Unetbootin/Unetbootin.desktop /usr/share/applications/
                                sudo cp Unetbootin/Unetbootin.png /usr/share/icons/   
                                rm -rf 'AACuBEC58iO1vLxoUz5FuZKOa?dl=0' && rm -Rf Unetbootin                                       			 
				;;
			   47)
			        #Install  EtcherAppImage 
			        echo "================ Установка EtcherAppImage ================"
			        wget https://www.dropbox.com/sh/dajkpg6rwof3ru2/AACn4uvLuERyraJVZNio42nAa?dl=0 && \
				unzip -q 'AACn4uvLuERyraJVZNio42nAa?dl=0'                                                                           
                                chmod +x Etcher/EtcherApp.desktop
                                sudo mv Etcher/EtcherApp.desktop /usr/share/applications/   
                                sudo mv Etcher/Etcher.png /usr/share/icons/ 
                                chmod +x ./Etcher/*.AppImage
				sudo mv Etcher/balenaEtcher*.AppImage /opt/BalenaEtcher.AppImage
                                rm -rf 'AACn4uvLuERyraJVZNio42nAa?dl=0' && rm -Rf Etcher     		         
			       ;;
			  48)
			        #Install RosaImageWriter
                                echo "================ Установка RosaImageWriter ================"
				wget https://www.dropbox.com/sh/dr3j9cee5db2wk6/AABV9sSW_anWxoZMqws9iLqoa?dl=0 && \
				unzip -q 'AABV9sSW_anWxoZMqws9iLqoa?dl=0'                               
                                chmod +x ./RosaIW/RosaImageWriter
                                sudo cp RosaIW/RosaImageWriter /opt/                                
                                chmod +x RosaIW/RosaImageWriter.desktop
                                sudo cp RosaIW/RosaImageWriter.desktop /usr/share/applications/
                                sudo cp RosaIW/RosaImageWriter.png /usr/share/icons/   
                                rm -rf 'AABV9sSW_anWxoZMqws9iLqoa?dl=0' && rm -Rf RosaIW
			       ;;			
			  49)
			        #Install LumQtAppImage   
			        echo "================ Установка LumQtAppImage ================"
			        wget https://www.dropbox.com/sh/4ni1snhnlq6ejcd/AACXlV8GNSyoObTKxFyb2_caa?dl=0 && \
				unzip -q 'AACXlV8GNSyoObTKxFyb2_caa?dl=0'			 				                                                                 
				chmod +x LumQtApp/LumQtApp.desktop                                                                                                                                                                          
                                sudo mv LumQtApp/LumQtApp.desktop /usr/share/applications/
                                sudo mv LumQtApp/LumQt.png /usr/share/icons/
                                mv LumQtApp/MXLiveUsbMaker.sh ~/'Рабочий стол'/
				chmod +x ./LumQtApp/*.AppImage 
				sudo mv LumQtApp/live-usb-maker-qt*.AppImage /opt/LumQt.AppImage
				rm -rf 'AACXlV8GNSyoObTKxFyb2_caa?dl=0' && rm -Rf LumQtApp                             
			       ;;
		          50)
                                #Install MultiBootUSB
				echo "================ Установка MultiBootUSB ================"
                                wget https://www.dropbox.com/sh/tfngu17tt3dnj4p/AADNU6lEkYmi35rFMJPsNVeRa?dl=0 && \
				unzip -q 'AADNU6lEkYmi35rFMJPsNVeRa?dl=0'
                                sudo dpkg -i MultiBootUSB/python3-multibootusb*
				sudo apt install -fy                                 					 
                                chmod +x MultiBootUSB/multibootusb.desktop                                                              
                                sudo cp MultiBootUSB/multibootusb.desktop /usr/share/applications/
                                sudo cp MultiBootUSB/MultiBootUsb.svg /usr/share/icons/   
                                rm -rf 'AADNU6lEkYmi35rFMJPsNVeRa?dl=0' && rm -Rf MultiBootUSB 
			       ;;
			  51)
			        #Install VentoyWebTar
			        echo "================ Установка VentoyWebTar ================"  
				wget https://www.dropbox.com/sh/8xszka2m3dtyugy/AAD7pz3NICDHsOMkungofVeCa?dl=0 && \
				unzip -q 'AAD7pz3NICDHsOMkungofVeCa?dl=0'
				tar xvf VentoyWeb/ventoy* 
				sudo mv ventoy* /opt/ventoy
				sudo chown -R $USER:$USER /opt/ventoy			                                                                             
                                chmod +x VentoyWeb/VentoyWeb.desktop  
                                sudo cp VentoyWeb/VentoyWeb.desktop /usr/share/applications/   
                                sudo cp VentoyWeb/Ventoy.png /usr/share/icons/   
                                rm -rf 'AAD7pz3NICDHsOMkungofVeCa?dl=0' && rm -Rf VentoyWeb                        			 
			       ;;
			  52)
			        #Install Mintstick
		                echo "================ Установка Mintstick ======================"
				sudo apt install -y mintstick 
                                wget https://www.dropbox.com/sh/6bz3gmomboihu7j/AACErhxW5xoPNi3vQEX3hq0ua?dl=0 && \
				unzip -q 'AACErhxW5xoPNi3vQEX3hq0ua?dl=0'				                             
                                chmod +x Mintstick/mintstick.desktop
                                chmod +x Mintstick/mintstick-format.desktop
                                sudo cp Mintstick/mintstick.desktop /usr/share/applications/
                                sudo cp Mintstick/mintstick-format.desktop /usr/share/applications/  
                                sudo cp Mintstick/RecordsStick.svg /usr/share/icons/   
                                sudo cp Mintstick/FormatStick.svg /usr/share/icons/                                                             				  
				chmod +x Mintstick/sysctl.conf
                                sudo cp Mintstick/sysctl.conf /etc/sysctl.conf 
                                rm -rf 'AACErhxW5xoPNi3vQEX3hq0ua?dl=0' && rm -Rf Mintstick                         		       				                                                                   				 
			       ;;
			  53)
			        #Install CapacityTester   
			        echo "================ Установка CapacityTester ================"
			        wget https://www.dropbox.com/sh/61u0gag8i9au1m1/AAAzprmaVblT-j44jUnNtgVAa?dl=0 && \
				unzip -q 'AAAzprmaVblT-j44jUnNtgVAa?dl=0'
				chmod +x CapacityTester/CapacityTester  
				sudo cp CapacityTester/CapacityTester /opt/                                                                 
				chmod +x CapacityTester/CapacityTester.desktop                                                                                                                                                                          
                                sudo cp CapacityTester/CapacityTester.desktop /usr/share/applications/
                                sudo cp CapacityTester/CapacityTester.svg /usr/share/icons/
				rm -rf 'AAAzprmaVblT-j44jUnNtgVAa?dl=0' && rm -Rf CapacityTester                                                            
			       ;;
			  54)
			        #Install DDRescue-GUI
			        echo "================ Установка DDRescue-GUI ================"  
                                wget https://www.dropbox.com/sh/wzzgx3j35lziyhz/AABapMGhtZVW3KvRec8gcbB2a?dl=0 && \
				unzip -q 'AABapMGhtZVW3KvRec8gcbB2a?dl=0'
                                sudo dpkg -i DDRescueGUI/*.deb
				sudo apt install -fy                                                                               
				mv DDRescueGUI/ddrescue-gui.sh ~/'Рабочий стол'/
                                rm -rf 'AABapMGhtZVW3KvRec8gcbB2a?dl=0' && rm -Rf DDRescueGUI  		       
			       ;;
			  55)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"                    
			       ;;
			  56)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"
			       ;;
			  57)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"			       
			       ;;
			  58)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"
			       ;;
			  59)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"                                
			       ;;
			  60)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================" 
			       ;;
			  61)
			       #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"         	                                
			       ;;
			  62)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================" 
			       ;;
			  63)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"                               
			       ;;
			  64)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================" 				 
			       ;;
			  65)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"          
			       ;;
		          66)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"                           
			       ;;
			  67)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"                    
			       ;;
			  68)
                                #Install Cutter
			        echo "================ Установка Cutter ================"
                                wget https://www.dropbox.com/sh/q9umcsqs866sv8l/AACD2TXDiZ--vmanc23_xOeGa?dl=0 && \
				unzip -q 'AACD2TXDiZ--vmanc23_xOeGa?dl=0' 
                                chmod +x Cutter/Cutter*.AppImage
                                sudo mv Cutter/Cutter*.AppImage /opt/Cutter.AppImage                                                                       
                                chmod +x Cutter/CutterAppImage.desktop                                                                                                               
                                sudo cp Cutter/CutterAppImage.desktop /usr/share/applications/
                                sudo cp Cutter/Cutter.png /usr/share/icons/  
				cp Cutter/Cutter.sh ~/'Рабочий стол'/    
				tar xvf Cutter/rizin.tar.gz
				mv rizin ~/.config/rizin                       
                                rm -rf 'AACD2TXDiZ--vmanc23_xOeGa?dl=0' && rm -Rf Cutter			
			       ;;
			  69)
			        #Install Gotop
				echo "================ Установка Gotop ================"
                                wget https://www.dropbox.com/sh/d5ugl4dc43jcmxs/AADSkJBfQdCiJP4Io_8uNmcNa?dl=0 && \
				unzip -q 'AADSkJBfQdCiJP4Io_8uNmcNa?dl=0'
                                chmod +x Gotop/gotop				
				sudo cp Gotop/gotop /opt/ 
                                cp Gotop/GotopInstall.sh ~/'Рабочий стол'/
				sudo cp Gotop/Gotop.png /usr/share/icons/
                                chmod +x Gotop/Gotop.desktop
                                cp Gotop/Gotop.desktop /usr/share/applications/                                                                   				  
                                rm -rf 'AADSkJBfQdCiJP4Io_8uNmcNa?dl=0' && rm -Rf Gotop                            
			       ;;
			  70)
			        #Install gImageReader
			        echo "================ Установка gImageReader ================"
				sudo apt install -y gimagereader tesseract-ocr-osd tesseract-ocr-rus tesseract-ocr-eng
                                wget https://www.dropbox.com/sh/om8mkd01k6rouua/AADXp3fQj2SeYjiGURt9MpQIa?dl=0 && \
				unzip -q 'AADXp3fQj2SeYjiGURt9MpQIa?dl=0'
                                mv GImageReader/gImageReader.sh ~/'Рабочий стол'/
                                rm -rf 'AADXp3fQj2SeYjiGURt9MpQIa?dl=0' && rm -Rf GImageReader
			       ;;
		          71)
			        #Install Дополнительные драйвера Wi-Fi Marvell и NXP (Libertas)
			        echo "================ Установка Wi-Fi Marvell и NXP (Libertas) ============="	
                                sudo apt install -y firmware-linux firmware-linux-nonfree firmware-libertas 	                         	 				                                  
			       ;;
                          72)
			        #Install Дополнительные драйвера Wi-Fi Intel
				echo "================ Установка Wi-Fi intel =========================="
                                sudo apt install -y firmware-linux firmware-linux-nonfree firmware-iwlwifi
                               ;;
                          73)
			        #Install Дополнительные драйвера Wi-Fi realtek
                                echo "================ Установка Wi-Fi realtek ========================="
				sudo apt install -y firmware-linux firmware-linux-nonfree firmware-realtek 
                               ;;
                          74)
			        #Install Дополнительные драйвера Wi-Fi atheros
				echo "================ Установка Wi-Fi atheros ================"
                                sudo apt install -y firmware-linux firmware-linux-nonfree firmware-atheros
                               ;;
                          75)
			       #Install Дополнительные драйвера Wi-Fi broadcom
			        echo "================ Установка Дополнительные драйвера Wi-Fi broadcom ================" 
                                sudo apt install -y firmware-linux firmware-linux-nonfree broadcom-sta-dkms     
                               ;;
                          76)
			        #Install ClipGrabAppImage
			        echo "================ Установка ClipGrabAppImage ================"				 
                                wget https://www.dropbox.com/sh/jxap3clejvvii2e/AAC61QrPWcy7X9FbSVMnrVeGa?dl=0 && \
				unzip -q 'AAC61QrPWcy7X9FbSVMnrVeGa?dl=0'		         		       
                                chmod +x ClipGrabApp/ClipGrabApp.desktop
                                sudo mv ClipGrabApp/ClipGrabApp.desktop /usr/share/applications/
                                sudo mv ClipGrabApp/ClipGrabApp.png /usr/share/icons/  
                                mv ClipGrabApp/ClipGrab.sh ~/'Рабочий стол'/
				chmod +x ./ClipGrabApp/*.AppImage
				sudo mv ClipGrabApp/ClipGrab*.AppImage /opt/ClipGrab.AppImage
				rm -rf 'AAC61QrPWcy7X9FbSVMnrVeGa?dl=0' && rm -Rf ClipGrabApp		       		 			       
                               ;;
                          77)
			        #Install XDM
				echo "================ Установка XDM ================"
				wget https://www.dropbox.com/sh/9xwhut480sofbjd/AAAdfIqtIhWjOdOKPMG76RL0a?dl=0 && \
				unzip -q 'AAAdfIqtIhWjOdOKPMG76RL0a?dl=0'
                                cp XDM/XtremeDownloadManager.sh ~/'Рабочий стол'/
				tar xvf XDM/xdm-setup*.tar.xz
				sudo sh install.sh
				rm -rf 'AAAdfIqtIhWjOdOKPMG76RL0a?dl=0' && rm -Rf XDM
				rm -rf install.sh
				rm -rf readme.txt
                               ;;
                          78)
			        #Install YoutubeDlDlpGui
			        echo "================ Установка YoutubeDlDlpGui ================"
                                sudo apt install -y youtube-dl yt-dlp youtubedl-gui aria2   
				wget https://www.dropbox.com/sh/vxwefpe3kz91rxe/AAAdvdmnkLQc4JLI2hy6W1vQa?dl=0 && \
				unzip -q 'AAAdvdmnkLQc4JLI2hy6W1vQa?dl=0'
				cp yt-dlp/YoutubeDlDlpGui.sh ~/'Рабочий стол'/
				rm -rf 'AAAdvdmnkLQc4JLI2hy6W1vQa?dl=0' && rm -Rf yt-dlp                                                                                              	
                               ;;
                          79)
			        #Install YTFZF
			        echo "================ YTFZF ================"
                                sudo apt install -y curl jq fzf mpv python3-distutils-extra chafa dmenu imv vlc youtube-dl ytfzf \
				python3-attr python3-docopt python3-pil python3-xlib python3 libc6 libx11-6 libxext6
				wget https://www.dropbox.com/sh/6nb2l797rk5mfhf/AACUxOh6ljJ_oy3vw_Yo6ahLa?dl=0 && \
				unzip -q 'AACUxOh6ljJ_oy3vw_Yo6ahLa?dl=0'                             
				mv YTFZF/YTFZF.sh ~/'Рабочий стол'/			
				rm -rf 'AACUxOh6ljJ_oy3vw_Yo6ahLa?dl=0' && rm -Rf YTFZF 
                               ;;
                          80)
			        #Install JDownloader2 
				echo "================ Установка JDownloader2 ================"
                                sudo apt install -y default-jdk
                                wget https://www.dropbox.com/sh/wv25tzc9utsmfzd/AAAcfrdv3iS3Kg_O2rGqAE_Ha?dl=0 && \
				unzip -q 'AAAcfrdv3iS3Kg_O2rGqAE_Ha?dl=0'	
				cp Descargas/JDownloader2.sh ~/'Рабочий стол'/
		                chmod +x Descargas/JD*
				sudo sh Descargas/JD*.sh
                                chmod +x Descargas/'JDownloader 2-0.desktop'
                                sudo cp Descargas/'JDownloader 2-0.desktop' /usr/share/applications/
                                rm -rf 'AAAcfrdv3iS3Kg_O2rGqAE_Ha?dl=0' && rm -Rf Descargas
                               ;;
                          81)
			        #Install VideomassAppimage 
				echo "================ Установка VideomassAppimage ================"
                                sudo apt install -y ffmpeg youtube-dl yt-dlp	
                                wget https://www.dropbox.com/sh/6dfe6qouezk9vy2/AABkGkuT3L6w3QsdLhbVlRqSa?dl=0 && \
				unzip -q 'AABkGkuT3L6w3QsdLhbVlRqSa?dl=0'
                                chmod +x VideomassApp/VideomassApp.desktop
                                sudo mv VideomassApp/VideomassApp.desktop /usr/share/applications/ 
                                sudo mv VideomassApp/VideomassApp.png /usr/share/icons/ 
				mv VideomassApp/VideomassChannel.sh ~/'Рабочий стол'/
				chmod +x ./VideomassApp/*.AppImage
				sudo mv VideomassApp/Videomass*.AppImage /opt/Videomass.AppImage                               			 				
				rm -rf 'AABkGkuT3L6w3QsdLhbVlRqSa?dl=0' && rm -Rf VideomassApp                            
                               ;;
                          82)
			        #Install MediaDownloader
				echo "================  Установка MediaDownloader ================"
                                sudo apt install -y media-downloader yt-dlp
				wget https://www.dropbox.com/sh/2vzwr29ywi8dctq/AABBYlekZpn_qGxB1OR6rVbea?dl=0 && \
				unzip -q 'AABBYlekZpn_qGxB1OR6rVbea?dl=0'
                                tar xvf MediaDownloader/media.tar.gz 
                                mv media-downloader ~/.config/media-downloader
                                rm -rf 'AABBYlekZpn_qGxB1OR6rVbea?dl=0' && rm -Rf MediaDownloader
                               ;;
                          83)
			        #Install MotrixApp
			        echo "================ MotrixApp ================"
				wget https://www.dropbox.com/sh/0jgherokzw8un8t/AABGT5Kp9L9N2di9UFKsxdQza?dl=0 && \
				unzip -q 'AABGT5Kp9L9N2di9UFKsxdQza?dl=0'				
				chmod +x MotrixApp/MotrixApp.desktop
                                sudo mv MotrixApp/MotrixApp.desktop /usr/share/applications/   
                                sudo mv MotrixApp/MotrixApp.png /usr/share/icons/
				chmod +x ./MotrixApp/*.AppImage				                                          
				sudo mv MotrixApp/Motrix*.AppImage /opt/Motrix.AppImage   
				mv MotrixApp/Motrix.sh ~/'Рабочий стол'/
				tar xvf MotrixApp/Motrix.tar.gz  
				mv Motrix ~/.config/Motrix                                                    
				rm -rf 'AABGT5Kp9L9N2di9UFKsxdQza?dl=0' && rm -Rf MotrixApp                   		 
                               ;;
                          84)
			        #Install Persepolis 
				echo "================ Установка Persepolis ================"	
                                sudo apt install -y aria2 persepolis
				wget https://www.dropbox.com/sh/t5zggq9dzns19ap/AAANV8d7XXMxzympXfUWMo31a?dl=0 && \
				unzip -q 'AAANV8d7XXMxzympXfUWMo31a?dl=0'
                                mv Persepolis/Persepolis.png ~/'Рабочий стол'/
                                mv Persepolis/Persepolis.sh ~/'Рабочий стол'/
                                rm -rf 'AAANV8d7XXMxzympXfUWMo31a?dl=0' && rm -Rf Persepolis 
                               ;;
                          85)
			        #Install AppImageUpdate
				echo "================ Установка AppImageUpdate ================"      
				wget https://www.dropbox.com/sh/0jjoic2u2qiu2gl/AABso4fr297VkkQEuGcFuZz3a?dl=0 && \
				unzip -q 'AABso4fr297VkkQEuGcFuZz3a?dl=0'			
				chmod +x AppImageUpdate/AppImageUpdate.desktop
				sudo mv AppImageUpdate/AppImageUpdate.desktop  /usr/share/applications/
				sudo mv AppImageUpdate/AppImageUpdate.png /usr/share/icons/
				mv AppImageUpdate/AppImageUpdate.sh ~/'Рабочий стол'/ 
				chmod +x ./AppImageUpdate/*.AppImage
				sudo mv AppImageUpdate/AppImageUpdate*.AppImage  /opt/AppImageUpdate.AppImage				
				rm -rf 'AABso4fr297VkkQEuGcFuZz3a?dl=0' && rm -Rf AppImageUpdate
                               ;;
                          86)
			        #Install Detect It EasyDeb
				echo "================ Установка Detect It EasyDeb================"
				wget https://www.dropbox.com/sh/530b2baiwt6865n/AADPn5dNjksXn7uexKcmxvCfa?dl=0 && \
				unzip -q 'AADPn5dNjksXn7uexKcmxvCfa?dl=0'                               
                                sudo dpkg -i DetectItEasy/*.deb
                                sudo apt install -fy     
				mv DetectItEasy/DetectItEasy.sh ~/'Рабочий стол'/
                                rm -rf 'AADPn5dNjksXn7uexKcmxvCfa?dl=0' && rm -Rf DetectItEasy 
                               ;;
                          87)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"                          
                               ;;
                          88)
			        #Install Pluma
				echo "================ Установка Pluma ================"
                                sudo apt install -y pluma
				wget https://www.dropbox.com/sh/h95quyaq50u9337/AABLLDhjc_XNOH8ZzXF_vwQTa?dl=0 && \
				unzip -q 'AABLLDhjc_XNOH8ZzXF_vwQTa?dl=0'
				tar xvf  Pluma/pluma.tar.gz
				mv pluma ~/.config/pluma			
				rm -rf 'AABLLDhjc_XNOH8ZzXF_vwQTa?dl=0' && rm -Rf Pluma
                               ;;
                          89)
			        #Install MeditAppImage
				echo "================ Установка MeditAppImage ================"
				wget https://gitlab.com/Chebur70/Setting/-/raw/main/MeditAppimage.tar.gz
				tar xvf MeditAppimage.tar.gz && rm -rf MeditAppimage.tar.gz
				chmod +x Medit/Medit.AppImage	
				sudo mv Medit/Medit.AppImage /usr/local/bin/
				chmod +x Medit/MeditAppImage.desktop
                                sudo mv Medit/MeditAppImage.desktop /usr/share/applications/ 
                                sudo mv Medit/medit.png /usr/share/icons/ 						
				tar xvf Medit/medit.tar.gz
                                mv medit ~/.local/share/medit
                                rm -Rf Medit                                                                  
                               ;;
                          90)
			        #Install Leafpad
		                echo "================ Установка Leafpad ================"
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/leafpad_0.8.18.1-5_amd64.deb
				sudo dpkg -i leafpad*
				sudo apt install -f -y
				rm -rf leafpad*  
                                wget https://www.dropbox.com/sh/ublhpdts1wlskau/AAD6ELxDOj4LVGtsFCg4Y0PDa?dl=0 && \
				unzip -q 'AAD6ELxDOj4LVGtsFCg4Y0PDa?dl=0'
                                tar xvf Leafpad/leafpad.tar.gz
                                mv leafpad ~/.config/ 
                                sudo cp Leafpad/Leafpad.png /usr/share/icons/
                                rm -rf 'AAD6ELxDOj4LVGtsFCg4Y0PDa?dl=0' && rm -Rf Leafpad                                                   
                               ;;
                          91)
		                #Install Gedit
				echo "================ Установка Gedit ================"
				sudo apt install -y gedit
                                wget https://www.dropbox.com/sh/ye4pn6aattmdt0s/AAAM6euX_TeGh3yOP7hbXpsea?dl=0 && \
				unzip -q 'AAAM6euX_TeGh3yOP7hbXpsea?dl=0'
                                chmod +x Gedit/org.gnome.gedit.desktop
                                sudo cp Gedit/org.gnome.gedit.desktop /usr/share/applications/   
                                sudo cp Gedit/Gedit.svg /usr/share/icons/    
                                rm -rf 'AAAM6euX_TeGh3yOP7hbXpsea?dl=0' && rm -Rf Gedit                             		 
                               ;;
                          92)
			        #Install OpenOfficeApp
			        echo "================ Установка OpenOfficeApp ================"	
				wget https://www.dropbox.com/sh/inf0p3shz56dggm/AABiwJqm22j3PQg0qRf13pV7a?dl=0 && \
				unzip -q 'AABiwJqm22j3PQg0qRf13pV7a?dl=0'                                                                              
                                chmod +x OpenOfficeApp/OpenOfficeApp.desktop
                                sudo mv OpenOfficeApp/OpenOfficeApp.desktop /usr/share/applications/   
                                sudo mv OpenOfficeApp/OpenOffice.png /usr/share/icons/ 
				chmod +x ./OpenOfficeApp/*.AppImage 
				sudo mv OpenOfficeApp/OpenOffice*.AppImage /opt/OpenOffice.AppImage                         
                                rm -rf 'AABiwJqm22j3PQg0qRf13pV7a?dl=0' && rm -Rf OpenOfficeApp                             
                               ;;
                          93)
			        #Install FreeOfficeAppImage 
		                echo "================ Установка FreeOfficeAppImage ================"
			        wget https://www.dropbox.com/sh/pfwx54yeguqufpn/AABnFK9WNOUWlu_JEMwpC_Eca?dl=0 && \
				unzip -q 'AABnFK9WNOUWlu_JEMwpC_Eca?dl=0'
                                chmod +x FreeOffice/FreeOfficeApp.desktop
                                sudo mv FreeOffice/FreeOfficeApp.desktop /usr/share/applications/
                                sudo mv FreeOffice/FreeOffice.png /usr/share/icons/
				chmod +x ./FreeOffice/*.AppImage
				sudo mv FreeOffice/FreeOffice*.AppImage /opt/FreeOffice.AppImage
                                rm -rf 'AABnFK9WNOUWlu_JEMwpC_Eca?dl=0' && rm -Rf FreeOffice                                 			 	                              
                               ;;
                          94)
			        #Install OnlyOfficeAppImage
				echo "================ Установка OnlyOfficeAppImage ================"
				wget https://www.dropbox.com/sh/ptl3svv61irikb2/AADaNs8m9QVSPup3vrmbMfeVa?dl=0 && \
				unzip -q 'AADaNs8m9QVSPup3vrmbMfeVa?dl=0'
                                chmod +x OnlyOffice/OnlyOfficeApp.desktop
                                sudo mv OnlyOffice/OnlyOfficeApp.desktop /usr/share/applications/
                                sudo mv OnlyOffice/Onlyoffice.png /usr/share/icons/
				chmod +x ./OnlyOffice/*.AppImage
				sudo mv OnlyOffice/DesktopEditors*.AppImage /opt/DesktopEditors.AppImage
                                rm -rf 'AADaNs8m9QVSPup3vrmbMfeVa?dl=0' && rm -Rf OnlyOffice                                                                                
                               ;;
                          95)
		               #Install WPSOfficeAppImage 
		                echo "================ Установка WPSOfficeAppImage ================"
			        wget https://www.dropbox.com/sh/8uf25m1pti1ud3o/AACxBdMG6yX8F6nkchp4zfa-a?dl=0 && \
				unzip -q 'AACxBdMG6yX8F6nkchp4zfa-a?dl=0'
                                chmod +x WPSOffice/WPS-Office.desktop
                                sudo mv WPSOffice/WPS-Office.desktop /usr/share/applications/
                                sudo mv WPSOffice/wpsoffice.svg /usr/share/icons/
				chmod +x ./WPSOffice/*.AppImage
				sudo mv WPSOffice/WPS-Office*.AppImage /opt/WPS-Office.AppImage
                                rm -rf 'AACxBdMG6yX8F6nkchp4zfa-a?dl=0' && rm -Rf WPSOffice                         
                               ;;
                          96)
			        #Install ConkyAll 
			        echo "================ Установка ConkyAll ================"
                                sudo apt install -y conky-all
				wget https://www.dropbox.com/sh/fp9f9ek4cuecpfe/AABNfOcmlk0YnezVWxTjTN2Sa?dl=0 && \
				unzip -q 'AABNfOcmlk0YnezVWxTjTN2Sa?dl=0'
				tar xvf Conky/GreenDark.tar.gz
				sudo mv auzia-conky /opt/auzia-conky
				tar xvf Conky/scripts.tar.gz
				sudo mv scripts /opt/scripts
				tar xvf Conky/start_conky.desktop.tar.gz
				chmod +x start_conky.desktop
				sudo mv start_conky.desktop /etc/xdg/autostart/				
				chmod +x Conky/conky.desktop
                                sudo cp Conky/conky.desktop /etc/xdg/autostart/		                                 
				tar xvf Conky/GrannySmithApple.tar.gz
                                sudo cp -r conky /etc/ && rm -Rf conky
				mv Conky/ConkyREADME.sh ~/'Рабочий стол'/			
                                rm -rf 'AABNfOcmlk0YnezVWxTjTN2Sa?dl=0' && rm -Rf Conky
                               ;;
                          97)
			        #Install Gnome Pie
			        echo "================ Установка Gnome Pie ================"
			        sudo apt install -y gnome-pie
			        wget https://www.dropbox.com/sh/wtctfwmzlxoe9u7/AADDOkDbMEn-Cm96_e9w3wBGa?dl=0 && \
				unzip -q 'AADDOkDbMEn-Cm96_e9w3wBGa?dl=0'
			        cp GnomePie/GnomePie.sh ~/'Рабочий стол'/
			        rm -rf 'AADDOkDbMEn-Cm96_e9w3wBGa?dl=0' && rm -Rf GnomePie 	                                                       
                               ;;
                          98)
			        #Install Plank
			        echo "================ Установка Plank ================"
			        sudo apt install -y plank                                                                
                               ;;
                          99)				 
			        #Install Cairo-dock
			        echo "================ Установка Cairo-dock ================"
                                sudo apt install -y cairo-dock
				wget https://www.dropbox.com/sh/so0lexf3aft7diz/AAAJnuUJJa7cUQUIUk2flPwma?dl=0 && \
				unzip -q 'AAAJnuUJJa7cUQUIUk2flPwma?dl=0'
                                chmod +x CairoDock/cairo-dock.desktop
                                cp CairoDock/cairo-dock.desktop ~/.config/autostart/                                 
				tar xvf CairoDock/Bcairo-dock.tar.gz
                                cp -r cairo-dock ~/.config/ && rm -Rf cairo-dock
                                rm -rf 'AAAJnuUJJa7cUQUIUk2flPwma?dl=0' && rm -Rf CairoDock		                              
                               ;;
                        100)
			        #Install Речевой Сервер Speech Dispatcher
				echo "================ Установка Речевой Сервер Speech Dispatcher ================"
				sudo apt install -y speech-dispatcher speech-dispatcher-audio-plugins speech-dispatcher-espeak-ng
                                sudo apt install -y espeak espeak-data espeak-ng-data espeakup libespeak-ng1 libespeak1 orca                                
				wget https://www.dropbox.com/sh/61eox35lnu7m7es/AAAG_jvUgmCiIEe9LlnWFblPa?dl=0 && \
				unzip -q 'AAAG_jvUgmCiIEe9LlnWFblPa?dl=0'
                                chmod -R 777 Orca
                                sudo cp Orca/espeak.desktop /usr/share/applications/
                                cp Orca/orca-autostart.desktop ~/.config/autostart/ 
                                sudo cp Orca/Espeak.png /usr/share/icons/
                                sudo cp Orca/orca.png /usr/share/icons/
                                rm -rf 'AAAG_jvUgmCiIEe9LlnWFblPa?dl=0' && rm -Rf Orca                             				                              
                               ;;
                        101)
			        #Install Из каталога deb*
		                echo "================ Установка Из каталога deb ======================"
		                cd deb/
		                sudo dpkg -i *.deb
		                sudo apt install -f -y
		                cd ../


	    esac
	done

echo "Запуск обновления плагинов"
sleep 1s
sudo apt install -y xfce4-goodies xfce4-battery-plugin xfce4-clipman xfce4-clipman-plugin \
xfce4-cpufreq-plugin xfce4-datetime-plugin xfce4-diskperf-plugin xfce4-fsguard-plugin \
xfce4-genmon-plugin xfce4-mount-plugin xfce4-xkb-plugin xfce4-sensors-plugin \
xfce4-smartbookmark-plugin xfce4-timer-plugin xfce4-wavelan-plugin \
xfce4-power-manager-plugins xfce4-pulseaudio-plugin

#Установка программ из репозиториев.
echo "Начало установки программ из репозиториев."
sleep 1s
sudo apt install -y menulibre uget vlc vlc-l10n git grub-customizer \
gnome-disk-utility synaptic     
#GpartedAplPng
wget https://www.dropbox.com/sh/mktrqz4if039hfd/AAAKSQnaPSsfmTuxjAMNUJ4Ya?dl=0 && \
unzip -q 'AAAKSQnaPSsfmTuxjAMNUJ4Ya?dl=0'
sudo dpkg -i Gparted/gparted*.deb
sudo apt install -fy
chmod +x Gparted/gparted.desktop
sudo cp Gparted/Gparted.png /usr/share/icons/
cp Gparted/gparted.desktop ~/.local/share/applications/
rm -rf 'AAAKSQnaPSsfmTuxjAMNUJ4Ya?dl=0' && rm -Rf Gparted                                                
#Доп утилиты
#sudo apt install -y gnome-system-tools network-manager-gnome \
#wireless-tools qt5ct qt5-gtk-platformtheme qt5-style-plugins
echo 'Установка дополнительных программ по мере изучения Linux'
sudo apt install -y spacefm spacefm-common tumbler mtpaint \
thunar-volman hardinfo lshw lshw-gtk seahorse psensor gtkhash \
thunar-gtkhash celluloid mpv winff preload neofetch dbus-x11 partitionmanager htop 
#Transmission
wget https://www.dropbox.com/sh/y6xvctjm5r9a6o3/AABZEq7tq3rNX9O0RLaIppIza?dl=0 && \
unzip -q 'AABZEq7tq3rNX9O0RLaIppIza?dl=0'
sudo dpkg -i transmission/transmission*.deb
sudo apt install -fy
rm -rf 'AABZEq7tq3rNX9O0RLaIppIza?dl=0' && rm -Rf transmission 
#Игры
#sudo apt install -y aisleriot foobillardplus kpat
#Добавление в автостарт
#GuakeTerminal
sudo apt install -y guake
wget https://www.dropbox.com/sh/2g2wdsyyg3bc42l/AADXlO6rG2zswOpVZbe-FQgCa?dl=0 && \
unzip -q 'AADXlO6rG2zswOpVZbe-FQgCa?dl=0'
chmod +x Guake/guake.desktop
sudo cp Guake/GuakeTerminal.png /usr/share/icons/
cp Guake/guake.desktop ~/.config/autostart/		       
rm -rf 'AADXlO6rG2zswOpVZbe-FQgCa?dl=0' && rm -Rf Guake
#QBittorrent
sudo apt install -y qbittorrent
wget https://www.dropbox.com/sh/83ftoqo026abqgg/AABm3SnbVcFxwL3kMhpIcB8da?dl=0 && \
unzip -q 'AABm3SnbVcFxwL3kMhpIcB8da?dl=0'
chmod +x QBittorrent/org.qbittorrent.qBittorrent.desktop
sudo cp QBittorrent/org.qbittorrent.qBittorrent.desktop /usr/share/applications/	
sudo cp QBittorrent/QBittorrent.png /usr/share/icons/	       
rm -rf 'AABm3SnbVcFxwL3kMhpIcB8da?dl=0' && rm -Rf QBittorrent
#PulseAudioAlsa
echo 'Устанавливаем звук от  pulseaudio'  #https://www.freedesktop.org/wiki/Software/PulseAudio/
sleep 1s
sudo apt install pulseaudio gstreamer1.0-pulseaudio pulseaudio-utils xfce4-pulseaudio-plugin \
pavucontrol libpulse-mainloop-glib0 libpulse0 libpulsedsp -y
echo 'Устанавливаем звук от Alsa'   #https://alsa-project.org/wiki/Main_Page
sudo apt install alsa-utils gstreamer1.0-alsa libasound2 libasound2-data libasound2-plugins -y
wget https://gitlab.com/Chebur70/Setting/-/raw/main/PulseAudioAlsa.tar.gz
tar xvf PulseAudioAlsa.tar.gz && rm -rf PulseAudioAlsa.tar.gz
chmod +x PulseAudioAlsa/Pulseaudio.desktop
sudo cp PulseAudioAlsa/Pulseaudio.desktop /usr/share/applications/
sudo cp PulseAudioAlsa/Pulseaudio.desktop /etc/xdg/autostart/
sudo cp PulseAudioAlsa/PulseAudio.svg /usr/share/icons/
rm -Rf PulseAudioAlsa
#Hv3WebBrowser-Applications  
wget https://www.dropbox.com/sh/hw7poa8emh7861n/AADZ3FErDJ3PWgBZfE-qq_uXa?dl=0 && \
unzip -q 'AADZ3FErDJ3PWgBZfE-qq_uXa?dl=0'
chmod +x Browser/hv3.desktop
sudo cp Browser/hv3.desktop /usr/share/applications/ 
sudo mv Browser/Hv3WebBrowser.png /usr/share/icons/
rm -rf 'AADZ3FErDJ3PWgBZfE-qq_uXa?dl=0' && rm -Rf Browser
#Установка заголовочных файлов ядра   http://rus-linux.net/MyLDP/kernel/kernel-headers.html
sudo apt install -y linux-headers-$(uname -r|sed 's,[^-]*-[^-]*-,,')
#Установка настроек внешнего вида
#OptSh
wget https://gitlab.com/Chebur70/Setting/-/raw/main/OptSh.tar.gz
tar xvf OptSh.tar.gz && rm -rf OptSh.tar.gz
chmod -R 777 opt
sudo cp -R opt / && rm -Rf opt
#OptApl
wget https://gitlab.com/Chebur70/Setting/-/raw/main/OptApl.tar.gz
tar xvf OptApl.tar.gz && rm -rf OptApl.tar.gz
chmod -R 777 OptApl
sudo cp OptApl/РедактированиеSourcesList.desktop /usr/share/applications/ 
sudo cp OptApl/DebInstall.desktop /usr/share/applications/ 
sudo cp OptApl/Release.desktop /usr/share/applications/  
sudo cp OptApl/TecmintMonitor.desktop /usr/share/applications/ 
sudo cp OptApl/Xkill.desktop /usr/share/applications/ 
sudo cp OptApl/UpgradePrelink.desktop /usr/share/applications/ 
sudo cp OptApl/LocalesTzdata.desktop /usr/share/applications/ 
sudo cp OptApl/Neofetch.desktop /usr/share/applications/ 
sudo cp OptApl/SystemUpgrade.desktop /usr/share/applications/
rm -Rf OptApl
#OptPng
wget https://gitlab.com/Chebur70/Setting/-/raw/main/OptPng.tar.gz
tar xvf OptPng.tar.gz && rm -rf OptPng.tar.gz
chmod -R 777 OptPng
sudo cp -R OptPng/icons /usr/share/
rm -Rf OptPng
#Install MOCP-MusicOnConsolePlayer
wget https://www.dropbox.com/sh/980q1ps3l86oi2a/AAAfyy_SABXnaMRDSNrrJ7uja?dl=0 && \
unzip -q 'AAAfyy_SABXnaMRDSNrrJ7uja?dl=0'
sudo dpkg -i MOCP/moc*.deb
sudo apt install -fy
chmod +x MOCP/Moc.desktop
sudo cp MOCP/Moc.desktop /usr/share/applications/
sudo cp MOCP/Moc.png /usr/share/icons/
cp MOCP/Accept-AmamosLaVida.mp3 ~/'Рабочий стол'/ 
cp MOCP/MOCP.sh ~/'Рабочий стол'/
rm -rf 'AAAfyy_SABXnaMRDSNrrJ7uja?dl=0' && rm -Rf MOCP
#Install Prelink для ускорения запуска программ в Linux			 
wget https://www.dropbox.com/sh/3zaw6kt99g2rstx/AACFXjA4sG9raoxhU8zSxi8Wa?dl=0 && \
unzip -q 'AACFXjA4sG9raoxhU8zSxi8Wa?dl=0'
sudo dpkg -i Prelink/*.deb
sudo apt install -fy
rm -rf 'AACFXjA4sG9raoxhU8zSxi8Wa?dl=0' && rm -Rf Prelink
#LibreofficeAplPng
wget https://www.dropbox.com/sh/5b4vlk0phzsvi3f/AAA1kT9j_a1_tOSTp4jDPR9ta?dl=0 && \
unzip -q 'AAA1kT9j_a1_tOSTp4jDPR9ta?dl=0'
chmod -R 777 LibreOffice                                    
sudo cp LibreOffice/libreoffice-draw.desktop /usr/share/applications/     
sudo cp LibreOffice/libreoffice-math.desktop /usr/share/applications/         
sudo cp LibreOffice/libreoffice-writer.desktop /usr/share/applications/
sudo cp LibreOffice/libreoffice-calc.desktop /usr/share/applications/  
sudo cp LibreOffice/libreoffice-impress.desktop /usr/share/applications/  
sudo cp LibreOffice/libreoffice-startcenter.desktop /usr/share/applications/				
sudo cp -r LibreOffice/icons /usr/share/
rm -rf 'AAA1kT9j_a1_tOSTp4jDPR9ta?dl=0' && rm -Rf LibreOffice
#Install MousepadAplPng
wget https://www.dropbox.com/sh/3sp3cqvbs8i6b4n/AAAXqoYoz8YaC_YakjxJaRAWa?dl=0 && \
unzip -q 'AAAXqoYoz8YaC_YakjxJaRAWa?dl=0'
chmod +x Mousepad/org.xfce.mousepad.desktop
sudo cp Mousepad/org.xfce.mousepad.desktop /usr/share/applications/
sudo cp Mousepad/Mousepad.png /usr/share/icons/
rm -rf 'AAAXqoYoz8YaC_YakjxJaRAWa?dl=0' && rm -Rf Mousepad
#Install FirefoxESR
wget https://www.dropbox.com/sh/5fffc86mpq4dxua/AABEUmmp8wULRoKP2yanxTRka?dl=0 && \
unzip -q 'AABEUmmp8wULRoKP2yanxTRka?dl=0'
chmod +x Firefox/firefox-esr.desktop
sudo cp Firefox/firefox-esr.desktop /usr/share/applications/
sudo cp Firefox/Firefox.svg /usr/share/icons/
tar xvf Firefox/mozilla.tar.gz
rm -rf 'AABEUmmp8wULRoKP2yanxTRka?dl=0' && rm -Rf Firefox
#ПрозрачныйФонIcons
wget https://gitlab.com/Chebur70/Setting/-/raw/main/gtkrc-2.0.tar.gz
tar xvf gtkrc-2.0.tar.gz && rm -rf gtkrc-2.0.tar.gz
#Рабочий стол и дополнение
wget https://gitlab.com/Chebur70/Setting/-/raw/main/Desktop.tar.gz
tar xvf Desktop.tar.gz && rm -rf Desktop.tar.gz
cp РабочийСтол/Компьютер.desktop ~/'Рабочий стол'/
cp РабочийСтол/КтоИспользуетDebian.sh ~/'Рабочий стол'/
cp РабочийСтол/ПаролиLiveссылки.ctb.pdf ~/'Рабочий стол'/
cp РабочийСтол/MOCP-MusicOnConsolePlayer.sh ~/'Рабочий стол'/
rm -Rf РабочийСтол
#Install KlichalexWeb
wget https://www.dropbox.com/sh/wwmeaedbrimnbsl/AAAKupA10I71b5HilCdXsspIa?dl=0 && \
unzip -q 'AAAKupA10I71b5HilCdXsspIa?dl=0'
chmod +x Klichalex/KlichalexF.desktop
cp Klichalex/KlichalexF.desktop ~/'Рабочий стол'/ 
sudo mv Klichalex/KlichalexF.desktop /usr/share/applications/
sudo mv Klichalex/Klichalex.jpg /usr/share/icons/
rm -rf 'AAAKupA10I71b5HilCdXsspIa?dl=0' && rm -Rf Klichalex
#LogrotateJournald позволяет уменьшить размер директории до указанного размера
wget https://www.dropbox.com/sh/5eoxbngrwjyik34/AABJvFInBr3U7n9WcEgFGl1Xa?dl=0 && \
unzip -q 'AABJvFInBr3U7n9WcEgFGl1Xa?dl=0'
sudo mv Journald/journald.conf /etc/systemd/
sudo mv Journald/logrotate.conf /etc/
sudo mv Journald/rsyslog /etc/logrotate.d/
sudo systemctl restart systemd-journald
rm -rf 'AABJvFInBr3U7n9WcEgFGl1Xa?dl=0' && rm -Rf Journald
#Внешний Вид
cp -r appearance/xfce4 ~/.config/
cp -r appearance/panel ~/.config/
#прописываем владельца каталога xfce4
chown -R ${name}:${name} ~/.config/xfce4/
chown -R ${name}:${name} ~/.config/panel/
#удаление папки appearance
rm -Rf appearance

echo "Давайте настроим автоматический запуск системы."
echo "(Если Вы, конечно, это хотите)"
echo "===================================="
echo ""
echo ""
echo "Автоматический вход в Xfce настраивается путем редактирования файла «/etc/lightdm/lightdm.conf»."
echo "Для этого необходимо найти в пункте [Seat:*] две строки :"
echo "#autologin-user="
echo "#autologin-user-timeout=0"
echo "И изменить их, раскоментировать и прописать свой логин, например:"
echo "autologin-user=alex  #вместо alex Ваш логин"
echo "autologin-user-timeout=0"
echo "Сохранить изменения конфига"
echo "Запустить lightdm.conf для редактирования?"
echo "y - запустить, любой другой символ - нет"
read doing 
case $doing in
y)
 sudo geany /etc/lightdm/lightdm.conf
 ;;
*)
 echo 'Редактирование lightdm.conf отменено'
esac #окончание оператора case.
echo ""
echo ""

echo 'Обновление и очистка после всех установок'
sudo apt full-upgrade -y
sudo apt autoremove -y
#очищает папку /var/cache/apt/archives;
sudo aptitude clean -y
#Очистка APT кеш 
sudo apt clean -y

echo "Скрипт работу закончил."
echo "Спасибо klichalex автору скрипта Transformation! https://www.youtube.com/user/klichalex/featured"
echo "Спасибо klichalex автору хороших видео уроков! https://rutube.ru/channel/23628980/videos/"
echo "Спасибо klichalex автору перевода Refracta на Русский язык http://prostolinux.my1.ru/"
echo "Спасибо circulosmeos https://github.com/circulosmeos за предоставленые скрипты"
echo "Спасибо разработчику скриптов Dmitriy wj42ftns Chekhov, Russia https://gist.github.com/wj42ftns"
echo "Спасибо kachnu https://github.com/kachnu за предоставленые скрипты"
echo "Спасибо Systemback https://gitlab.com/Kendek/systemback"
echo "Спасибо LeCorbeau's Vault https://lecorbeausvault.wordpress.com/2021/01/10/quickly-build-a-custom-bootable-installable-debian-live-iso-with-live-build/"
echo "Спасибо Франко Кониди https://syslinuxos.com/things-to-do-after-installing-syslinuxos-12/"
echo "Нет недостижимых целей,есть высокий коэффициент лени,недостаток смекалкии и запас отговорок"
echo "Не важно насколько медленно ты движешься, главное не останавливаться.Конфуций"
echo "Наш ответ Чемберлену!Дави Империализма Гиену Могучий Рабочий Класс!" 
echo "Вчера были танки лишь у Чемберлена,А нынче есть и у нас!"
echo "Запомните,чтобы ничего не делать, надо уметь делать все" 
echo "Я не потерпел неудачу, я нашел 10 000 способов, которые не сработают-Эдисон."
echo "Операция настройки системы завершена"
echo "Выйти из настроек, или перезапустить систему?"
echo "y - выйти, любой другой символ - перезапуск"
read doing 
case $doing in
y)
  exit
 ;;
*)
sudo reboot -f
esac #окончание оператора case.
